# Show Images
A very simple GUI application written in Python 3 that shows images to the user.

## Usage
The GUI uses TkInter and `tk.Tk()` is called when the window is initialized.
This means that a window has to be created before any additional TkInter 
dependent code is executed.

Set the `images` attribute to add images to be shown in the window. The images 
attribute should be a list of images.

Call the `run` method to show the window and start the TkInter main loop.

### Example
```
from showimages.window import Window

import tkinter as tk
from PIL import ImageTk, Image

win = Window()
dir = 'images/'
file_names = [dir + 'img1.jpg', dir + 'img2.jpg', dir + 'img3.jpg']

images = []
for name in file_names:
	curr_img = ImageTk.PhotoImage(Image.open(name))
	images.append(curr_img)

win.images = images
win.run()
```
