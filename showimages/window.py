# -*- coding: UTF-8 -*-

import tkinter as tk

class Window(tk.Frame):
	"""
	Window for showing images to the user. Call the run method to show the window. 
	Note that the Window will still be running in the same thread and therefore
	block execution of postceding code until closed. 
	
	Only one instance is intended to be used at a time.
	"""

	def __init__(self):
		"""
		Creates a new Window instance with a basic user interface 
		and with an empty image list.
		"""
		super().__init__(tk.Tk())
		self.master.title("Show Images")
		self.master.resizable(False, False)

		self.previous = self.__create_button("Previous", self.previous)
		self.next = self.__create_button("Next", self.next)

		self.img_holder = tk.Label(self.master)
		self.img_holder.pack()

		self.__images = []
		self.__img_index = 0


	def __create_button(self, text, command):
		"""
		Creates a button with the given text and command.

		:param str text: The button text
		:param callable command: The command to be executed by the button
		:return: The newly created button
		:rtype: tkinter.Button
		"""
		button = tk.Button(self.master, text = text, command = command)
		button.pack()
		return button

	def run(self):
		"""
		Starts the main loop and shows the window.
		"""
		self.update_image()
		self.master.mainloop()

	def update_image(self):
		"""
		Updates the image holder with the current image.
		"""
		if len(self.images) > 0:
			self.img_holder["image"] = self.images[self.__img_index]
			self.img_holder.pack()

	def next(self):
		"""
		Set the next image to the current one.
		"""
		if self.__img_index < len(self.images) - 1:
			self.__img_index += 1
			self.update_image()

	def previous(self):
		"""
		Sets the previous image as the current one.
		"""
		if self.__img_index > 0:
			self.__img_index -= 1
			self.update_image()

	def get_images(self):
		"""
		Gets the image list.

		:return: The image list
		:rtype: list
		"""
		return self.__images

	def set_images(self, images):
		"""
		Sets the image list.

		:param list images: The new image list
		"""
		self.__images = images
		self.update_image()

	images = property(get_images, set_images)

